# code-with-quarkus project

keytool -genkeypair -keystore src/main/resources/keystore/keystore.jks -dname "CN=environment-service, OU=Development, O=Stonemountain.dk, L=Horsens, ST=Denmark, C=DK" -storepass mypassword -keyalg RSA -alias localhost -ext SAN=dns:localhost

## get a access token

export access_token=$(\
    curl -X POST https://auth.stonemountain.dk/auth/realms/demo/protocol/openid-connect/token \
    --user demo-universe:50270521-dc9b-44a0-859f-48bcd481399e \
    -H 'content-type: application/x-www-form-urlencoded' \
    -d 'username=demo&password=HappyGoLucky&grant_type=password' | jq --raw-output '.access_token' \
)
 
curl --insecure -v -H 'ACCEPT: application/json' https://localhost:8443/environment/variables -H "Authorization: Bearer "$access_token
