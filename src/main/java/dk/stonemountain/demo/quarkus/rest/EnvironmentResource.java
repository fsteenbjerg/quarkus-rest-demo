package dk.stonemountain.demo.quarkus.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/environment")
public class EnvironmentResource {
    @GET
    @Path("variables")
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("demo-user")
    public List<EnvironmentVariable> getVariables() {
    	return System.getenv().entrySet().stream()
    		.map(e -> new EnvironmentVariable(e.getKey(), e.getValue()))
    		.collect(Collectors.toList());
    }
}