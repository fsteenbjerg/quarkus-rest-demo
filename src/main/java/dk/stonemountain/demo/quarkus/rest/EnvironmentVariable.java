package dk.stonemountain.demo.quarkus.rest;

public class EnvironmentVariable {
	public String key;
	public String value;
	
	public EnvironmentVariable(String key, String value) {
		this.key = key;
		this.value = value;
	}
}
